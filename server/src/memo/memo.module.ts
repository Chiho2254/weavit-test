import { Module } from '@nestjs/common';
import { MemoController } from './memo.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MemoEntity } from './memo.entity';
import { MemoService } from './memo.service';
import { UserEntity } from '../user/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([MemoEntity, UserEntity])
  ],
  controllers: [MemoController],
  providers: [MemoService]
})
export class MemoModule {}

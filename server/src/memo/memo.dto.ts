import { IsNotEmpty } from 'class-validator';
import { UserRO } from 'src/user/user.dto';

export class MemoDTO {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    content: string;
}

export class MemoRO {
    memoId: string;
    created_at: Date;
    updated_at: Date;
    title: string;
    content: string;
    user: UserRO;
  }
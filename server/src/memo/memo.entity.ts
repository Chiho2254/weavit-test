import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm';
import { UserEntity } from 'src/user/user.entity';


@Entity()
export class MemoEntity {
    @PrimaryGeneratedColumn('uuid')
    memoId: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @Column('text')
    content: string;

    @Column('text')
    title: string;

    @Column('uuid')
    userId: string;

    @ManyToOne(type => UserEntity, user => user.userId)
    user: UserEntity;
}